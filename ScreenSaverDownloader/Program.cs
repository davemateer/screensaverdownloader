﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Threading;

namespace ScreenSaverDownloader
{
    class Program
    {
        static void Main()
        {
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 4; j++)
                {
                    var filename = "b" + i + "-" + j + ".mov";
                    var url = "http://a1.v2.phobos.apple.com.edgesuite.net/us/r1000/000/Features/atv/AutumnResources/videos/" + filename;

                    var success = FileDownloader.DownloadFile(url, @"d:\temp\movies\" + filename, 1000000);
                    Console.WriteLine("Done  - success: " + success);
                }
            }
            Console.ReadLine();
        }
    }

    class FileDownloader
    {
        private readonly string url;
        private readonly string fullPathWhereToSave;
        private bool result;
        private readonly SemaphoreSlim semaphore = new SemaphoreSlim(0);

        DateTime lastUpdate;
        long lastBytes;

        public FileDownloader(string url, string fullPathWhereToSave)
        {
            this.url = url;
            this.fullPathWhereToSave = fullPathWhereToSave;
        }

        // Returns an instance of itself running StartDownload
        public static bool DownloadFile(string url, string fullPathWhereToSave, int timeoutInMilliSec)
        {
            return new FileDownloader(url, fullPathWhereToSave).StartDownload(timeoutInMilliSec);
        }

        public bool StartDownload(int timeout)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPathWhereToSave));

                if (File.Exists(fullPathWhereToSave))
                {
                    //File.Delete(fullPathWhereToSave);
                }
                else
                {
                    using (var client = new WebClient())
                    {
                        var ur = new Uri(url);
                        client.DownloadProgressChanged += WebClientDownloadProgressChanged;
                        client.DownloadFileCompleted += WebClientDownloadCompleted;
                        Console.WriteLine(@"Downloading file: " + ur);
                        client.DownloadFileAsync(ur, fullPathWhereToSave);
                        semaphore.Wait(timeout);
                        return result && File.Exists(fullPathWhereToSave);
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Was not able to download file!");
                Console.Write(e);
                return false;
            }
            finally { semaphore.Dispose(); }
        }

        private void WebClientDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Console.Write("\r     -->    {0}% {1}MB {2}MB", e.ProgressPercentage, e.BytesReceived / 1000000, e.TotalBytesToReceive / 1000000);
        }

        private void WebClientDownloadCompleted(object sender, AsyncCompletedEventArgs args)
        {
            result = !args.Cancelled;
            if (!result) Console.Write(args.Error.ToString());
            Console.WriteLine(Environment.NewLine + "Download finished!");
            semaphore.Release();
        }
    }
}

